package com.example.SoftarchApp.Passenger;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDao")
public class FakePassengerDataAccessService implements PassengerDao {

    private static List<PassengerModel> passengerDB = new ArrayList<>();

    @Override
    public int insertPassenger(UUID passengerID, PassengerModel passenger) {
        passengerDB.add(new PassengerModel(passengerID, passenger.getName(), passenger.getBanknumber()));
        return 1;
    }

    @Override
    public List<PassengerModel> selectAllPeople() {
        return passengerDB;
    }

    @Override
    public Optional<PassengerModel> selectPassengerById(UUID passengerID) {
        return passengerDB.stream()
                .filter(passenger -> passenger.getPassengerID().equals(passengerID))
                .findFirst();
    }

    @Override
    public int deletePassengerById(UUID passengerID) {
        return 0;
    }

    @Override
    public int updatePassengerById(UUID passengerID, PassengerModel passenger) {
        return 0;
    }
}
