package com.example.SoftarchApp.Passenger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping("Passengers")
@RestController
public class PassengerController {

    private final PassengerService passengerService;

    @Autowired
    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @PostMapping
    public void addPassenger(@RequestBody PassengerModel passenger) {
        passengerService.addPassenger(passenger);
    }

    @GetMapping
    public List<PassengerModel> getAllPeople(){
        return  passengerService.getAllPeople();
    }

    @GetMapping(path = "/{passengerID}")
    public PassengerModel getPassengerById(@PathVariable("passengerID") UUID passengerID){
        return passengerService.getPersonById(passengerID)
                .orElse(null);
    }
}
