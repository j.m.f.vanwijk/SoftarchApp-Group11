package com.example.SoftarchApp.Passenger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PassengerDao {

    int insertPassenger(UUID passengerID, PassengerModel passenger);

    default int insertPassenger(PassengerModel passenger) {
        UUID passengerID = UUID.randomUUID();
        return insertPassenger(passengerID, passenger);
    }

    List<PassengerModel> selectAllPeople();

    Optional<PassengerModel> selectPassengerById(UUID passengerID);

    int deletePassengerById(UUID passengerID);
    int updatePassengerById(UUID passengerID, PassengerModel passenger);
}
