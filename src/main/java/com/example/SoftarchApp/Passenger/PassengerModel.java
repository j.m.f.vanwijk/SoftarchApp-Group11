package com.example.SoftarchApp.Passenger;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class PassengerModel {

    private final UUID passengerID;
    private final String name;
    private final String banknumber;

    public PassengerModel(@JsonProperty("passengerID") UUID passengerID,
                          @JsonProperty("name") String name,
                          @JsonProperty("banknumber") String banknumber){
        this.passengerID = passengerID;
        this.name = name;
        this.banknumber = banknumber;
    }

    public UUID getPassengerID(){
        return passengerID;
    }

    public String getName(){
        return name;
    }

    public String getBanknumber() {
        return banknumber;
    }
}
