package com.example.SoftarchApp.Passenger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PassengerService {

    private final PassengerDao passengerDao;

    @Autowired
    public PassengerService(@Qualifier("fakeDao") PassengerDao passengerDao) {
        this.passengerDao = passengerDao;
    }

    public int addPassenger(PassengerModel passenger){
        return passengerDao.insertPassenger(passenger);
    }

    public List<PassengerModel> getAllPeople() {
        return passengerDao.selectAllPeople();
    }

    public Optional<PassengerModel> getPersonById(UUID passengerID){
        return  passengerDao.selectPassengerById(passengerID);
    }
}
