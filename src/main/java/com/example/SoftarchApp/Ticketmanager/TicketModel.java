package com.example.SoftarchApp.TicketManager;

import java.util.Date;
import java.util.UUID;

public class TicketModel {

    final private UUID ticketID;
    final private int beginstationID;
    final private int endstationID;
    final private Date date;

    public TicketModel(UUID ticketID, int beginstationID, int endstationID, Date date) {
        this.ticketID = ticketID;
        this.beginstationID = beginstationID;
        this.endstationID = endstationID;
        this.date = date;
    }

    public UUID getTicketID() {
        return ticketID;
    }

    public int getBeginstationID() {
        return beginstationID;
    }

    public int getEndstationID() {
        return endstationID;
    }

    public Date getDate() {
        return date;
    }
}
