package com.example.SoftarchApp.Tycoon;

import java.util.UUID;

public class TycoonModel {

    private final UUID tycoonID;
    private final String tycoonname;

    public TycoonModel(UUID tycoonid, String tycoonname) {
        this.tycoonID = tycoonid;
        this.tycoonname = tycoonname;
    }

    public UUID getTycoonid() {
        return tycoonID;
    }

    public String getTycoonname() {
        return tycoonname;
    }
}
